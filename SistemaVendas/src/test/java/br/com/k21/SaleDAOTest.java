package br.com.k21;

import org.junit.Test;

import br.com.k21.dao.SaleDAO;
import br.com.k21.infra.BaseDBTest;

public class SaleDAOTest extends BaseDBTest {

	@Test
	public void testeVendedorInexistente() {
		// Arrange
		Salesman salesman = new Salesman();
		salesman.setId(999);
		int ano = 2017;		
		double valorTotalVendasEsperado = 0;
		double valorTotalRetornado;
		
		// act
		SaleDAO.setEntityManager(emf.createEntityManager());
		valorTotalRetornado = SaleDAO.getSalesTotalBySalesmanAndYear(salesman, ano);
		
		// asserts
		assertEquals(valorTotalVendasEsperado, valorTotalRetornado);		
	}
		
	@Test
	public void testeVendedorUmRetorna142() {
		// Arrange
		Salesman salesman = new Salesman();
		salesman.setId(1);
		int ano = 2017;		
		double valorTotalVendasEsperado = 142;
		double valorTotalRetornado;
		
		// act
		SaleDAO.setEntityManager(emf.createEntityManager());
		valorTotalRetornado = SaleDAO.getSalesTotalBySalesmanAndYear(salesman, ano);
		
		// asserts
		assertEquals(valorTotalVendasEsperado, valorTotalRetornado);		
	}
}
