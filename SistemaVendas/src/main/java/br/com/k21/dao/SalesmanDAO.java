package br.com.k21.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.k21.Salesman;


public class SalesmanDAO {
	
    @PersistenceContext(unitName = "JPA")
    private static EntityManager entityManager;

    
    public static Salesman find(long id)
    {
        return entityManager.find(Salesman.class, new Long(id));
    }
    
    public static void remove(long id){
    	entityManager.remove(find(id));
    }
    
    public static List<Salesman> findVendedorByNome(String nome){
    	Query q = entityManager.createQuery("select s from Salesman s where s.name = :paramName");
    	
    	q.setParameter("paramName", nome);
    
    	List<Salesman> lista = q.getResultList();
    	return lista;
    }
    
    public static EntityManager getEntityManager()
    {
        return entityManager;
    }

    public static void setEntityManager(EntityManager entityManager)
    {
    
    	SalesmanDAO.entityManager = entityManager;
    }

}
