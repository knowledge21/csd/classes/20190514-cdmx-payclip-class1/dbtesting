package br.com.k21;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Sale
{
    @Id
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date saleDate;
    
    private Double value;
    
    private long salesmanId;
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}

	public long getVendedorId() {
		return salesmanId;
	}

	public void setVendedorId(long vendedorId) {
		this.salesmanId = vendedorId;
	}

}
